var gulp = require('gulp');
var babel = require('gulp-babel');

gulp.task('default', function() {
    console.log('gulp default. other options: TBC');
});

// http://www.dotnetcurry.com/javascript/1293/transpile-es6-modules-es5-using-babel
gulp.task('transpile', function () {
return gulp.src(['./dev/js/trendstop/**/*.js'])
.pipe(babel({
  "presets": ["es2015","react"]
}))
.pipe(gulp.dest('./assets/js/trendstop/'));
});

