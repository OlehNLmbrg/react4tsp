"use strict";

console.log('Story.js');

;(function ( namespace, undefined ) {
    
    namespace.Story = React.createClass({
        render: function() {
          //console.log(this.props.xyz_id);
          return (
          <div id="storyWrap">
            <div id="contentStory">
                    <h1>Story Content here, group_id={this.props.group_id}</h1>
            </div>
            <footer id="footerStory">
                    <h1>Story Footer (nav) here, group_id={this.props.group_id}</h1>
            </footer>
          </div>
          )
        }
      });

})(window.AFL.TSP.story.react = window.AFL.TSP.story.react || {});

