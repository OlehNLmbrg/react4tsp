"use strict";

console.log('global.js');

// trying reactJS within TSP
var AFL = {
    'host':null,
    'endpoints':{
        'local':{
            'story_nav':'/index_story.php?group_id=',
            'home_content_feed':'/index2.php'
        },
        'dev':{
            'story_nav':'/ajax/stories/nav?group_id=',
            'home_content_feed':'https://devpremium.trendstop.com/ajax/members/main?gender=w'
        },
    },
    'TSP':{
        'bookmarks':{
            
        },
        'story':{
            'react':{
                
            }
        }
    }
};

;(function ( namespace, undefined ) {
    
var host = window.location.hostname;
let myRe1 = /localhost/g;
let myRe2 = /devpremium\.trendstop\.com/g;
if(myRe1.test(host)){
    namespace.host = 'local';
}else if(myRe2.test(host)){
    namespace.host = 'dev';
}

})(window.AFL = window.AFL || {});